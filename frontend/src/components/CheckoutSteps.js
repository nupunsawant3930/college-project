import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Flex,
} from "@chakra-ui/react";
import { IoCaretForwardSharp } from "react-icons/io5";
import { Link as RouterLink } from "react-router-dom";

const CheckoutSteps = ({ step1, step2, step3, step4 }) => {
  return (
    <Flex justifyContent="center" mb="8">
      <Breadcrumb
        separator={<IoCaretForwardSharp color="gray.500" fontWeight="900" size='20px' />}>
        {/* Step 1 */}
        <BreadcrumbItem>
          {step1 ? (
            <BreadcrumbLink
              as={RouterLink}
              to="/login"
              fontWeight="900"
              textShadow="2px 2px white">
              Login
            </BreadcrumbLink>
          ) : (
            <BreadcrumbLink
              _disabled
              color="white"
              fontWeight="900"
              textShadow=" 2px 2px black">
              Login
            </BreadcrumbLink>
          )}
        </BreadcrumbItem>

        {/* Step 2 */}
        <BreadcrumbItem>
          {step2 ? (
            <BreadcrumbLink
              as={RouterLink}
              to="/shipping"
              fontWeight="900"
              textShadow="2px 2px white">
              Shipping
            </BreadcrumbLink>
          ) : (
            <BreadcrumbLink
              _disabled
              color="white"
              fontWeight="900"
              textShadow=" 2px 2px black">
              Shipping
            </BreadcrumbLink>
          )}
        </BreadcrumbItem>

        {/* Step 3 */}
        <BreadcrumbItem>
          {step3 ? (
            <BreadcrumbLink
              as={RouterLink}
              to="/payment"
              fontWeight="900"
              textShadow="2px 2px white">
              Payment
            </BreadcrumbLink>
          ) : (
            <BreadcrumbLink
              _disabled
              color="white"
              fontWeight="900"
              textShadow=" 2px 2px black">
              Payment
            </BreadcrumbLink>
          )}
        </BreadcrumbItem>

        {/* Step 4 */}
        <BreadcrumbItem>
          {step4 ? (
            <BreadcrumbLink
              as={RouterLink}
              to="/placeorder"
              fontWeight="900"
              textShadow="2px 2px white">
              Place Order
            </BreadcrumbLink>
          ) : (
            <BreadcrumbLink
              _disabled
              color="white"
              fontWeight="900"
              textShadow=" 2px 2px black">
              Place Order
            </BreadcrumbLink>
          )}
        </BreadcrumbItem>
      </Breadcrumb>
    </Flex>
  );
};

export default CheckoutSteps;
