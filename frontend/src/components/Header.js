import {
  Box,
  Button,
  Flex,
  Heading,
  Icon,
  Link,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
} from "@chakra-ui/react";
import { useState } from "react";
import { HiOutlineMenuAlt3, HiShoppingBag, HiUser } from "react-icons/hi";
import { useDispatch, useSelector } from "react-redux";
import { Link as RouterLink, useNavigate } from "react-router-dom";

import { IoChevronDown } from "react-icons/io5";
import { logout } from "../actions/userActions";
import HeaderMenuItem from "./HeaderMenuItem";

const Header = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [show, setShow] = useState(false);

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const logoutHandler = () => {
    dispatch(logout());
    navigate("/login");
  };

  return (
    <Flex
      as="header"
      alignItems="center"
      justifyContent="space-between"
      wrap="wrap"
      zIndex="99999"
      py="6"
      px="6"
      bgColor="#FFC2C3"
      borderWidth="5px"
      borderColor="black"
      borderstyle="rounded"
      w="100%"
      pos="fixed"
      top="0"
      left="0">
      {/* Title/Logo */}
      <Link as={RouterLink} to="/">
        <Heading
          as="h1"
          bgColor="#FFC2C3"
          fontSize="2xl"
          color="#FE7773"
          fontWeight="800"
          textDecor="none"
          _hover={{
            textDecor: "none",
            opacity: "0.7",
            textColor: " white",
            textShadow: " 1px 1px black",
          }}
          textShadow=" 1px 1px black">
          My Store
        </Heading>
      </Link>

      {/* Hamburger Menu */}
      <Box
        display={{ base: "block", md: "none" }}
        onClick={() => setShow(!show)}>
        <Icon as={HiOutlineMenuAlt3} color="white" w="6" h="6" />
      </Box>

      {/* Menu */}
      <Box
        display={{ base: show ? "block" : "none", md: "flex" }}
        width={{ base: "full", md: "auto" }}
        mt={{ base: 4, md: 0 }}>
        <HeaderMenuItem icon={HiShoppingBag} label="Cart" url="/cart" />

        {userInfo ? (
          <Menu>
            <MenuButton
              as={Button}
              rightIcon={<IoChevronDown />}
              bgColor="#FFC2C3"
              fontSize="xl"
              color="#FE7773"
              fontWeight="800"
              transition="all 0.2s"
              _hover={{
                textDecor: "none",
                opacity: "0.7",
                textColor: " white",
                textShadow: " 1px 1px black",
              }}
              textShadow=" 1px 1px black"
              // _expanded={{ bg: "#FE7773", color: "#FE7773" }}
              // _focus={{ boxShadow: "outline" }}
            >
              {userInfo.name}
            </MenuButton>
            <MenuList
              bgColor="#FFC2C3"
              border="2px"
              borderColor="#FE7773"
              borderWidth="2px"
              borderStyle="rounded">
              <MenuItem
                as={RouterLink}
                to="/profile"
                bgColor="#FFC2C3"
                fontSize="xl"
                fontWeight="600"
                color="#FE7773"
                _hover={{
                  textDecor: "none",
                  opacity: "0.7",
                  textColor: " white",
                }}>
                Profile
              </MenuItem>
              <MenuItem
                onClick={logoutHandler}
                bgColor="#FFC2C3"
                fontSize="xl"
                fontWeight="600"
                color="#FE7773"
                _hover={{
                  textDecor: "none",
                  opacity: "0.7",
                  textColor: " white",
                }}>
                Logout
              </MenuItem>
            </MenuList>
          </Menu>
        ) : (
          <HeaderMenuItem icon={HiUser} label="Login" url="/login" />
        )}

        {/* Admin Menu */}
        {userInfo && userInfo.isAdmin && (
          <Menu>
            <MenuButton
              ml="3"
              as={Button}
              bgColor="#FFC2C3"
              fontSize="xl"
              color="#FE7773"
              fontWeight="800"
              rightIcon={<IoChevronDown />}
              _hover={{
                textDecor: "none",
                opacity: "0.7",
                textColor: " white",
                textShadow: " 1px 1px black",
              }}
              textShadow=" 1px 1px black">
              Manage
            </MenuButton>
            <MenuList
              bgColor="#FFC2C3"
              border="2px"
              borderColor="#FE7773"
              borderWidth="2px"
              borderStyle="rounded">
              <MenuItem
                bgColor="#FFC2C3"
                fontSize="xl"
                fontWeight="600"
                color="#FE7773"
                as={RouterLink}
                to="/admin/userlist"
                _hover={{
                  textDecor: "none",
                  opacity: "0.7",
                  textColor: " white",
                }}>
                All Users
              </MenuItem>
              <MenuItem
                bgColor="#FFC2C3"
                fontSize="xl"
                fontWeight="600"
                color="#FE7773"
                as={RouterLink}
                to="/admin/productlist"
                _hover={{
                  textDecor: "none",
                  opacity: "0.7",
                  textColor: " white",
                }}>
                All Products
              </MenuItem>
              <MenuItem
                bgColor="#FFC2C3"
                fontSize="xl"
                fontWeight="600"
                color="#FE7773"
                as={RouterLink}
                to="/admin/orderlist"
                _hover={{
                  textDecor: "none",
                  opacity: "0.7",
                  textColor: " white",
                }}>
                All Orders
              </MenuItem>
            </MenuList>
          </Menu>
        )}
      </Box>
    </Flex>
  );
};

export default Header;
