import { Box, Flex, Heading, Image, Link, Text } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";

import Rating from "./Rating";

const ProductCard = ({ product }) => {
  return (
    <Link
      as={RouterLink}
      to={`/products/${product._id}`}
      _hover={{ textDecor: "none" }}>
      <Box
        borderRadius="lg"
        borderStyle="rounded"
        borderColor="#FE7773"
        
        borderWidth="10px"
        bgColor="#FFC2C3"
        _hover={{
          shadow: "2xl",
        }}>
        <Image
          src={product.image}
          alt={product.name}
          w="full"
          h="430px"
          objectFit="contain"
          border="rounded"
          borderColor="black"
          borderWidth="5px"
        />
        <Flex py="5" px="4" direction="column" justifyContent="space-between">
          <Heading
            as="h4"
            fontSize="2xl"
            // bgColor="#ffa4a6"
            // borderStyle="rounded"
            // borderWidth="5px"
            // borderColor="#ffa4a6"
            mb="3"
            color="black"
            fontWeight="900"
            _hover={{ textDecor: "underline" }}
            textShadow=" 2px 2px 2px white">
            {product.name}
          </Heading>
          <Flex alignItems="center" justifyContent="space-between">
            <Rating value={product.rating} color="yellow.500" />
            <Text
              fontSize="2xl"
              fontWeight="900"
              color="blue.600"
              textShadow=" 2px 2px  black">
              ₹{product.price}
            </Text>
          </Flex>
        </Flex>
      </Box>
    </Link>
  );
};

export default ProductCard;
