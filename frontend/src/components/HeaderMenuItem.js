import { Icon, Link } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";

const HeaderMenuItem = ({ label, url, icon }) => {
  return (
    <Link
      as={RouterLink}
      to={url}
      fontSize="xl"
      letterSpacing="wide"
      fontWeight="500"
      mr="5"
      display="flex"
      alignItems="center"
      color="#FE7773"
      _hover={{ textDecor: "none", color: "white" }}>
      <Icon as={icon} mr="1" w="4" h="4" />
      {label}
    </Link>
  );
};

export default HeaderMenuItem;
