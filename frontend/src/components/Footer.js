import { Flex, Text } from "@chakra-ui/react";

const Footer = () => {
  return (
    <Flex
      as="footer"
      justifyContent="center"
      py="5"
      bgColor="#ff9fa1"
      borderStyle="rounded"
      borderWidth="5px"
      borderColor="black">
      <Text
        fontSize="2xl"
        color="#FE7773"
        fontWeight="800"
        textDecor="underline"
        textShadow=" 1px 1px black"
        // bgColor="#fe5b56"
      >
        Copyright {new Date().getFullYear()}. RST Store. All Rights Reserved.
      </Text>
    </Flex>
  );
};

export default Footer;
