import { Alert, AlertIcon, AlertTitle } from "@chakra-ui/react";

const Message = ({ type = "info", children }) => {
  return (
    <Alert
      status={type}
      fontSize="xl"
      color="#FE7773"
      fontWeight="800"
      bgColor="#FFC2C3">
      <AlertIcon />
      <AlertTitle>{children}</AlertTitle>
    </Alert>
  );
};

export default Message;
