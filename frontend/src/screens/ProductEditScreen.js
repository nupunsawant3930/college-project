import {
  Button,
  Flex,
  Box,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Link,
  Spacer,
} from "@chakra-ui/react";
import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link as RouterLink, useNavigate, useParams } from "react-router-dom";
import { listProductDetails, updateProduct } from "../actions/productActions";
import FormContainer from "../components/FormContainer";
import Loader from "../components/Loader";
import Message from "../components/Message";
import { PRODUCT_UPDATE_RESET } from "../constants/productConstants";

const ProductEditScreen = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { id: productId } = useParams();

  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState("");
  const [brand, setBrand] = useState("");
  const [category, setCategory] = useState("");
  const [description, setDescription] = useState("");
  const [countInStock, setCountInStock] = useState("");

  const productDetails = useSelector((state) => state.productDetails);
  const { loading, error, product } = productDetails;

  const productUpdate = useSelector((state) => state.productUpdate);
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = productUpdate;

  useEffect(() => {
    if (successUpdate) {
      dispatch({ type: PRODUCT_UPDATE_RESET });
      navigate(`/admin/productlist`);
    } else {
      if (!product.name || product._id !== productId) {
        dispatch(listProductDetails(productId));
      } else {
        setName(product.name);
        setPrice(product.price);
        setImage(product.image);
        setBrand(product.brand);
        setCategory(product.category);
        setCountInStock(product.countInStock);
        setDescription(product.description);
      }
    }
  }, [dispatch, navigate, productId, product, successUpdate]);

  const submitHandler = (e) => {
    e.preventDefault();

    dispatch(
      updateProduct({
        _id: productId,
        name,
        price,
        image,
        brand,
        category,
        description,
        countInStock,
      })
    );
  };

  const uploadFileHandler = async (e) => {
    const file = e.target.files[0];
    const formData = new FormData();
    formData.append("image", file);

    try {
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      };

      const { data } = await axios.post(`/api/uploads`, formData, config);
      setImage(data);
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <>
      <Flex>
        <Button
          as={RouterLink}
          to="/admin/productlist"
          fontSize="lg"
          fontWeight="900">
          Go Back
        </Button>
      </Flex>

      <Flex w="full" alignItems="center" justifyContent="center" py="5">
        <Box rounded='lg' borderWidth='5px' borderColor='black'>
          <FormContainer>
            <Heading
              as="h1"
              mb="8"
              fontSize="3xl"
              fontWeight="900"
              textDecor="underline"
              textShadow="2px 3px white ">
              Edit Product
            </Heading>

            {loadingUpdate && <Loader />}
            {errorUpdate && <Message type="error">{errorUpdate}</Message>}

            {loading ? (
              <Loader />
            ) : error ? (
              <Message type="error">{error}</Message>
            ) : (
              <form onSubmit={submitHandler}>
                {/* NAME */}
                <FormControl id="name" isRequired>
                  <FormLabel
                    fontWeight="900"
                    fontSize="lg"
                    textShadow="2px 3px white ">
                    Name
                  </FormLabel>
                  <Input
                    type="text"
                    placeholder="Enter name"
                    fontWeight="900"
                    rounded="lg"
                    borderWidth="2px"
                    borderColor="black"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                </FormControl>
                <Spacer h="3" />

                {/* PRICE */}
                <FormControl id="price" isRequired>
                  <FormLabel
                    fontWeight="900"
                    fontSize="lg"
                    textShadow="2px 3px white ">
                    Price
                  </FormLabel>
                  <Input
                    type="number"
                    placeholder="Enter price"
                    rounded="lg"
                    borderWidth="2px"
                    borderColor="black"
                    fontWeight="900"
                    value={price}
                    onChange={(e) => setPrice(e.target.value)}
                  />
                </FormControl>
                <Spacer h="3" />

                {/* IMAGE */}
                <FormControl id="image" isRequired>
                  <FormLabel
                    fontWeight="900"
                    fontSize="lg"
                    textShadow="2px 3px white ">
                    Image
                  </FormLabel>
                  <Input
                    type="text"
                    placeholder="Enter image url"
                    rounded="lg"
                    borderWidth="2px"
                    borderColor="black"
                    value={image}
                    fontWeight="900"
                    onChange={(e) => setImage(e.target.value)}
                  />
                  <Input
                    type="file"
                    rounded="lg"
                    borderWidth="2px"
                    borderColor="black"
                    fontWeight="900"
                    onChange={uploadFileHandler}
                  />
                </FormControl>
                <Spacer h="3" />

                {/* DESCRIPTION */}
                <FormControl id="description" isRequired>
                  <FormLabel
                    fontWeight="900"
                    fontSize="lg"
                    textShadow="2px 3px white ">
                    Description
                  </FormLabel>
                  <Input
                    type="text"
                    placeholder="Enter description"
                    rounded="lg"
                    borderWidth="2px"
                    fontWeight="900"
                    borderColor="black"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </FormControl>
                <Spacer h="3" />

                {/* BRAND */}
                <FormControl id="brand" isRequired>
                  <FormLabel
                    fontWeight="900"
                    fontSize="lg"
                    textShadow="2px 3px white ">
                    Brand
                  </FormLabel>
                  <Input
                    type="text"
                    placeholder="Enter brand"
                    rounded="lg"
                    borderWidth="2px"
                    borderColor="black"
                    fontWeight="900"
                    value={brand}
                    onChange={(e) => setBrand(e.target.value)}
                  />
                </FormControl>
                <Spacer h="3" />

                {/* CATEGORY */}
                <FormControl id="category" isRequired>
                  <FormLabel
                    fontWeight="900"
                    fontSize="lg"
                    textShadow="2px 3px white ">
                    Category
                  </FormLabel>
                  <Input
                    type="text"
                    placeholder="Enter category"
                    rounded="lg"
                    borderWidth="2px"
                    borderColor="black"
                    fontWeight="900"
                    value={category}
                    onChange={(e) => setCategory(e.target.value)}
                  />
                </FormControl>
                <Spacer h="3" />

                {/* COUNT IN STOCK */}
                <FormControl id="countInStock" isRequired>
                  <FormLabel
                    fontWeight="900"
                    fontSize="lg"
                    textShadow="2px 3px white ">
                    Count In Stock
                  </FormLabel>
                  <Input
                    type="number"
                    placeholder="Product in stock"
                    rounded="lg"
                    borderWidth="2px"
                    borderColor="black"
                    fontWeight="900"
                    value={countInStock}
                    onChange={(e) => setCountInStock(e.target.value)}
                  />
                </FormControl>
                <Spacer h="3" />

                <Button
                  type="submit"
                  isLoading={loading}
                  colorScheme="teal"
                  mt="4">
                  Update
                </Button>
              </form>
            )}
          </FormContainer>
        </Box>
      </Flex>
    </>
  );
};

export default ProductEditScreen;
