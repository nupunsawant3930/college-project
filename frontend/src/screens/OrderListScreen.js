import {
  Box,
  Button,
  Flex,
  Heading,
  Icon,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { useEffect } from "react";
import { IoCloseCircleSharp } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { listOrders } from "../actions/orderActions";
import Loader from "../components/Loader";
import Message from "../components/Message";

const OrderListScreen = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const orderList = useSelector((state) => state.orderList);
  const { loading, error, orders } = orderList;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  console.log(orders);

  useEffect(() => {
    if (userInfo && userInfo.isAdmin) {
      dispatch(listOrders());
    } else {
      navigate("/login");
    }
  }, [dispatch, userInfo, navigate]);

  return (
    <>
      <Heading as="h1" fontSize="3xl" mb="5">
        Orders
      </Heading>

      {loading ? (
        <Loader />
      ) : error ? (
        <Message type="error">{error}</Message>
      ) : (
        <Box
          rounded="lg"
          shadow="lg"
          px="5"
          py="5"
          borderWidth="5px"
          borderColor="black"
          bgColor="#FFC2C3">
          <Table size="sm">
            <Thead>
              <Tr>
                <Th fontWeight="900" fontSize="lg">
                  ID
                </Th>
                <Th fontWeight="900" fontSize="lg">
                  USER
                </Th>
                <Th fontWeight="900" fontSize="lg">
                  DATE
                </Th>
                <Th fontWeight="900" fontSize="lg">
                  TOTAL PRICE
                </Th>
                <Th fontWeight="900" fontSize="lg">
                  PAID
                </Th>
                <Th fontWeight="900" fontSize="lg">
                  DELIVERED
                </Th>
                <Th></Th>
              </Tr>
            </Thead>
            <Tbody>
              {orders.map((order) => (
                <Tr key={order._id}>
                  <Td fontWeight="700" fontSize="md">
                    {order._id}
                  </Td>
                  <Td fontWeight="700" fontSize="md">
                    {order.user && order.user.name}
                  </Td>
                  <Td fontWeight="700" fontSize="md">
                    {order.createdAt.substring(0, 10)}
                  </Td>
                  <Td fontWeight="700" fontSize="md">
                    ₹{order.totalPrice}
                  </Td>
                  <Td fontWeight="700" fontSize="md">
                    {order.isPaid ? (
                      order.paidAt.substring(0, 10)
                    ) : (
                      <Icon
                        as={IoCloseCircleSharp}
                        color="red.600"
                        w="8"
                        h="8"
                      />
                    )}
                  </Td>
                  <Td>
                    {order.isDelivered ? (
                      order.deliveredAt.substring(0, 10)
                    ) : (
                      <Icon
                        as={IoCloseCircleSharp}
                        color="red.600"
                        w="8"
                        h="8"
                      />
                    )}
                  </Td>
                  <Td>
                    <Flex justifyContent="flex-end" alignItems="center">
                      <Button
                        mr="4"
                        as={RouterLink}
                        to={`/order/${order._id}`}
                        colorScheme="teal">
                        Details
                      </Button>
                    </Flex>
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </Box>
      )}
    </>
  );
};

export default OrderListScreen;
